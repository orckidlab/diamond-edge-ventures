/**
 * Build configuration
 *
 * @see {@link https://roots.io/docs/sage/ sage documentation}
 * @see {@link https://bud.js.org/guides/configure/ bud.js configuration guide}
 *
 * @typedef {import('@roots/bud').Bud} Bud
 * @param {Bud} app
 */
export default async (bud) => {
    const themeName = bud.env.get('TEXT_DOMAIN')
    bud
        /**
         * Application entrypoints
         * @see {@link https://bud.js.org/docs/bud.entry/}
         */
        .entry({
            app: ['@scripts/app', '@styles/app'],
            editor: ['@styles/editor']
        })

        /**
         * Enable VUE 3
         */
        .alias('vue', bud.path('@modules/vue/dist/vue.esm-bundler.js'))
        .define({
            __VUE_OPTIONS_API__: true,
            __VUE_PROD_DEVTOOLS__: false
        })

        // .alias('vue', bud.path('vue/dist/vue.esm-bundler.js'))
        // .define({
        //     __VUE_OPTIONS_API__: true,
        //     __VUE_PROD_DEVTOOLS__: false
        // })

        /**
         * Directory contents to be included in the compilation
         * @see {@link https://bud.js.org/docs/bud.assets/}
         */
        .assets(['images'])

        /**
         * Matched files trigger a page reload when modified
         * @see {@link https://bud.js.org/docs/bud.watch/}
         */
        .watch(['resources/views', 'app'])

        /**
         * Proxy origin (`WP_HOME`)
         * @see {@link https://bud.js.org/docs/bud.proxy/}
         */
        .proxy(`https://${themeName}.test`)

        /**
         * Development origin
         */
        .serve({
            url: 'https://localhost:3000',
            cert: `/Users/ashjoory/localhost.pem`,
            key: `/Users/ashjoory/localhost-key.pem`
        })

        /**
         * URI of the `public` directory
         * @see {@link https://bud.js.org/docs/bud.setPublicPath/}
         */
        .setPublicPath(`/app/themes/${themeName}/public/`)

        /**
         * Generate WordPress `theme.json`
         *
         * @note This overwrites `theme.json` on every build.
         *
         * @see {@link https://bud.js.org/extensions/sage/theme.json/}
         * @see {@link https://developer.wordpress.org/block-editor/how-to-guides/themes/theme-json/}
         */
        .wpjson.settings({
            appearanceTools: false,
            layout: {
                contentSize: '90%',
                wideSize: '1000px'
            },
            color: {
                custom: false,
                customDuotone: false,
                customGradient: false,
                defaultDuotone: false,
                defaultGradients: false,
                defaultPalette: false,
                duotone: null
            },
            custom: {
                spacing: {},
                typography: {
                    'font-size': {},
                    'line-height': {}
                }
            },
            spacing: {
                padding: true,
                units: ['px', '%', 'em', 'rem', 'vw', 'vh']
            },
            typography: {
                customFontSize: false
            }
        })
        .useTailwindColors()
        .useTailwindFontFamily()
        .useTailwindFontSize()
        .enable()
}
