<!doctype html>
<html <?php language_attributes(); ?> class="scroll-smooth">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, viewport-fit=cover">
  <?php
  wp_head();
  $googleFonts = 'https://fonts.googleapis.com/css2?&family=Roboto:wght@300;400;500;700&display=swap';
  ?>
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link rel="preload" as="style" href="<?php echo $googleFonts; ?>" referrerpolicy="no-referrer" />
  <link rel="stylesheet" href="<?php echo $googleFonts; ?>" media="print" onload="this.media='all'" referrerpolicy="no-referrer" />
  <meta name="google-site-verification" content="wFv7-6PX5E8BiFZ10biJ5uTgSWxp1_qojjcNy8WEXCM" />
</head>

<body data-application-name="<?php echo $_ENV["TEXT_DOMAIN"]; ?>" <?php body_class(array("bg-blue-300", "overflow-y-scroll", "font-roboto", "antialiased")); ?>>
  <?php wp_body_open(); ?>
  <?php do_action('get_header'); ?>

  <div id="app" class="bg-white min-h-screen">
    <?php echo view(app('sage.view'), app('sage.data'))->render(); ?>
  </div>

  <?php do_action('get_footer'); ?>
  <?php wp_footer(); ?>
</body>

</html>