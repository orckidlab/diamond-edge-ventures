export default {
    THUMBNAILS: {
        draggable: true,
        scrollLock: true,
        dots: false,
        arrows: {
            prev: '.glider-prev',
            next: '.glider-next'
        },
        responsive: [
            {
                // screens greater than >= 775px
                breakpoint: 775,
                settings: {
                    // Set to `auto` and provide item width to adjust to viewport
                    slidesToShow: '1.5',
                    slidesToScroll: '1'
                }
            },
            {
                // screens greater than >= 1024px
                breakpoint: 1024,
                settings: {
                    slidesToShow: 5,
                    slidesToScroll: 1
                }
            }
        ]
    },
    SINGLE: {
        draggable: false,
        scrollLock: true,
        slidesToShow: 1,
        duration: 0
    }
}
