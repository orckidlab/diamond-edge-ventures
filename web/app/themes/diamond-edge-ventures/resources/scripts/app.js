import { domReady } from '@roots/sage/client'
import { createApp } from 'vue'
import WyxosGlider from './components/wyxos-glider.vue'
import GLIDER_PRESETS from './presets/glider-presets.js'
import AppAccordion from './components/AppAccordion.vue'
import VueTippy from 'vue-tippy'

/**
 * jQuery is fed via WP
 */
try {
    window.$ = window.jQuery = require('jquery')
} catch (e) {}

/**
 * app.main
 */

const main = async (err) => {
    if (err) {
        // handle hmr errors
        console.error(err)
    }

    // vuejs
    const app = createApp({
        data() {
            return {
                applicationName: null,
                mobileNavigation: false,
                tab: 1
            }
        },
        async mounted() {
            const self = this

            // set application name
            const body = document.body
            this.applicationName = body.getAttribute('data-application-name')

            // mobile navigation
            let asideNavigation = $('.mobile-navigation')
            let asideNavigationItems = asideNavigation.find('.has-children > a')
            asideNavigationItems.on('click', function () {
                let $current = $(this)
                $current.find('svg').toggleClass('rotate-90')
                let $ul = $current.next('ul')
                if ($ul.length) {
                    $ul.toggleClass('block')
                    return false
                }
            })

            let asideNavigationMobileItems = asideNavigation.find('li > a');
            asideNavigationMobileItems.on('click', function () {
                $('.mobile-nav-close').trigger('click');
            })

            let winH = window.innerWidth;
            $('a[href*=\\#]:not([href=\\#])').on('click', function () {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.substr(1) + ']');
                if (target.length) {
                if (winH > 991) {
                    $('html,body').animate(
                    {
                        scrollTop: target.offset().top - 50,
                    },
                    100,
                    )
                } else {
                    $('html,body').animate(
                    {
                        scrollTop: target.offset().top - 35,
                    },
                    100,
                    )
                }
                return false;
                }
            })
        },
        methods: {
            toggleMobileNav() {
                this.mobileNavigation = !this.mobileNavigation
            },
            currentTab: function (tabNumber) {
                this.tab = tabNumber
            }
        }
    })

    app.config.productionTip = false
    app.config.globalProperties.$GLIDER_PRESETS = GLIDER_PRESETS

    app.component('WyxosGlider', WyxosGlider)
    app.component('AppAccordion', AppAccordion)

    app.use(VueTippy, {
        defaultProps: { placement: 'bottom', theme: 'light' }
    })

    app.mount('#app')
}

/**
 * Initialize
 *
 * @see https://webpack.js.org/api/hot-module-replacement
 */
domReady(main)
import.meta.webpackHot?.accept(main)
