<footer role="group" {{ $attributes->merge(['class' => 'flex justify-start flex-wrap mt-4 lg:mt-8 not-prose']) }}>
    {{ $slot }}
</footer>
