@switch($tag)
    @case($tag == 'button')
        <button type="button" {{ $attributes->merge(['class' => "$base $color"]) }}>
            {!! $title ?? $slot !!}
        </button>
    @break

    @default
        <a href="{{ $url }}" title="{{ $title }}" target="{{ $target }}"
            {{ $attributes->merge(['class' => "$base $color"]) }}>
            {!! $title ?? $slot !!}
        </a>
@endswitch
