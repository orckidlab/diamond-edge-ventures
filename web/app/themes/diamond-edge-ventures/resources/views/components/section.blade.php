<section role="doc-chapter" {{ $attributes->merge(['class' => 'px-8 py-16']) }}>
    <main class="container">
        {{ $slot }}
    </main>
</section>
