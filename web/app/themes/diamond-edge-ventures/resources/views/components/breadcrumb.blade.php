@if (!empty($items))
    <nav class="-mx-2 mb-6 hidden w-full items-center space-x-3 divide-x divide-black py-2 leading-3 xl:flex"
        typeof="BreadcrumbList" aria-label="Breadcrumb" vocab="https://schema.org/">
        @foreach ($items as $item)
            @if (empty($item['url']))
                <span class="cursor-default pl-3">
                    {!! $item['label'] !!}
                </span>
            @else
                <span class="pl-3" typeof="ListItem" property="itemListElement">
                    <a class="transition-colors hover:text-blue-700" typeof="WebPage" href="{{ $item['url'] }}"
                        title="Go to {!! $item['label'] !!}." property="item">
                        <span property="name">
                            @if ($loop->first)
                                @svg('images.icons.x-home', 'w-4 h-4', ['aria-label' => 'Home'])
                            @else
                                {!! $item['label'] !!}
                            @endif
                        </span>
                    </a>
                    <meta property="position" content="{{ $loop->iteration }}">
                </span>
            @endif
        @endforeach
    </nav>
@endif
