<article role="doc-part" {{ $attributes->merge(['class' => 'grid']) }}>
    {{ $slot }}
</article>