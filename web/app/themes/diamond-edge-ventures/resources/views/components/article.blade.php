<article role="doc-part"
    {{ $attributes->merge(['class' => 'prose 2xl:prose-xl max-w-none heir-ul:!pl-4 max-sm:heir-br:hidden']) }}>
    {{ $slot }}
</article>
