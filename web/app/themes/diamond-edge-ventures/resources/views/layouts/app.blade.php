@include('sections.mobile-navigation')

@include('sections.header')

<main id="main-content" role="document">
    @yield('content')
</main>

@hasSection('sidebar')
    <aside class="sidebar">
        @yield('sidebar')
    </aside>
@endif

@include('sections.footer')
