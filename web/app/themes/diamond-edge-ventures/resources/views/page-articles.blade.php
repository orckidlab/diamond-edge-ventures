{{--
  Template Name: Articles
--}}

@extends('layouts.app')

@section('content')
    @if ($hero)
        @include('sections.hero')
    @endif
    <x-section class="bg-blue-300 max-sm:px-0">
        <x-article class="text-center">
            <h3 class="max-sm:px-8">Articles</h3>
            @php
                $posts = $postsByCategory(3, 'articles');
            @endphp
            @if ($posts)
                <x-grid
                    class="not-prose !mt-12 grid-cols-1 gap-6 gap-y-6 max-sm:relative max-sm:flex max-sm:w-full max-sm:snap-x max-sm:snap-mandatory max-sm:overflow-x-auto max-sm:px-8 md:grid-cols-2 lg:grid-cols-3">
                    @foreach ($posts as $post)
                        @include('partials.post', [
                            'postUrl' => $post->url,
                            'postTitle' => $post->title,
                            'postImage' => $post->thumbnail,
                        ])
                    @endforeach
                </x-grid>
            @endif
        </x-article>
    </x-section>
    <x-section class="bg-blue-300 !pt-0 max-sm:px-0">
        <hr
            class="before:bg-hr relative mb-20 h-px border-none before:absolute before:top-0 before:right-[5%] before:left-[5%] before:h-px before:w-11/12 before:content-['']">
        <x-article class="text-center">
            <h3 class="max-sm:px-8">News</h3>
            @php
                $posts = $postsByCategory(12, 'news');
            @endphp
            @if ($posts)
                <x-grid
                    class="not-prose !mt-12 grid-cols-1 gap-6 gap-y-20 max-sm:relative max-sm:flex max-sm:w-full max-sm:snap-x max-sm:snap-mandatory max-sm:overflow-x-auto max-sm:px-8 md:grid-cols-2 lg:grid-cols-3">
                    @foreach ($posts as $post)
                        @include('partials.post', [
                            'postUrl' => $post->url,
                            'postTitle' => $post->title,
                            'postImage' => $post->thumbnail,
                        ])
                    @endforeach
                </x-grid>
            @endif
        </x-article>
    </x-section>
@endsection
