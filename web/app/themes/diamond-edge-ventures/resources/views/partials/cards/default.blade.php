<x-grid class="gap-4 md:grid-cols-3 md:gap-8">
    @foreach ($pages as $page)
        <a class="bg-split-white-gray prose lg:prose-lg group flex max-w-none flex-col p-5 xl:p-10"
            href="{{ get_permalink($page->ID) }}" title="{{ $page->post_title }}">
            <figure class="not-prose !mb-0">
                <picture
                    class="mask mask-hexagon bg-{{ $color ?? 'amber-400' }} flex aspect-square flex-col content-center items-center justify-center text-white transition-all group-hover:opacity-90">
                    @if ($thumbnailURL($page->ID, ''))
                        <img class="h-full w-full object-cover" src="{{ $thumbnailURL($page->ID, 'medium_large') }}"
                            alt="{{ $page->post_title }}" loading="lazy">
                    @else
                        @svg('images.icons.x-beesting', 'w-full h-full max-w-[30%] object-cover', ['aria-label' => 'default icon'])
                    @endif
                </picture>
            </figure>
            <h3 class="after:!content-none">{{ $page->post_title }}</h3>
            <p class="mb-8">{{ $page->post_excerpt }}</p>
            <x-buttons class="!mt-auto">
                <x-button tag="button">
                    Read more
                </x-button>
            </x-buttons>
        </a>
    @endforeach
</x-grid>
