<form class="flex w-full" role="search" method="get" action="{{ esc_url(home_url('/')) }}">
    <label class="sr-only mb-2 text-sm font-medium text-gray-900 dark:text-white" for="default-search">Search</label>
    <fieldset class="relative w-full">
        <div class="pointer-events-none absolute inset-y-0 left-0 flex items-center pl-3">
            <svg class="h-5 w-5 text-gray-500" aria-hidden="true" fill="none" stroke="currentColor" viewBox="0 0 24 24"
                xmlns="http://www.w3.org/2000/svg">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2"
                    d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z"></path>
            </svg>
        </div>
        <input
            class="block w-full rounded-lg border border-gray-300 bg-gray-50 p-4 pl-10 text-sm text-gray-900 focus:border-amber-400"
            id="default-search" name="s" type="search" value="{{ get_search_query() }}"
            placeholder="{{ $placeholder ?? 'Search' }}" required>
        <button
            class="hover:bg-dark-900 absolute right-2.5 bottom-2.5 rounded-lg bg-amber-400 px-4 py-2 text-sm font-medium text-white"
            type="submit" value="{{ esc_attr_x('Search', 'submit button') }}">Search</button>
    </fieldset>
</form>
