@if (count($items))
    <ul class="{{ $class ?? '' }} divide-y transition-all will-change-auto">
        @foreach ($items as $item)
            @php($item = (object) $item)
            <li
                class="child-ul:bg-{{ $item->classes ?? '' }} {{ count($item->children) ? 'has-children child-ul:hidden' : '' }} {{ $item->active ? 'active' : '' }} hover:bg-gray-100">
                <a class="flex w-full items-center justify-between px-5 py-3 text-lg font-medium"
                    href="{{ $item->url }}">
                    <span>{!! $item->label !!}</span>
                    @if (count($item->children))
                        @svg('images.icons.x-arrow', 'w-4 h-4', ['aria-label' => 'more links'])
                    @endif
                </a>
                @if (count($item->children))
                    @include('partials.nav-list', [
                        'items' => $item->children,
                        'class' => 'sub-nav pl-3 !py-0 heir-a:!text-sm child-li:hover:!bg-transparent',
                    ])
                @endif
            </li>
        @endforeach
    </ul>
@endif
