<x-section class="!py-4">
    @php
        $breadcrumb = $breadcrumb ?? true;
    @endphp
    @if ($breadcrumb)
        <x-breadcrumb></x-breadcrumb>
    @endif
    <header class="prose lg:prose-xl max-w-none">
        <h1>{!! $title !!}</h1>
    </header>
</x-section>
