{{-- {{ dd($buttons) }} --}}

<x-buttons>
    @foreach ($buttons as $button)
        @php($button = (object) $button)
        @if ($button->type === 'a-external')
            <x-button title="{{ $button->title }}" url="{{ $button->url }}" target="_blank"
                color="{{ $button->color }}">
                {{ $button->title }}
            </x-button>
        @elseif($button->type === 'button')
            <x-button title="{{ $button->title }}" url="{{ $button->section_id }}" target=""
                color="{{ $button->color }}">
                {{ $button->title }}
            </x-button>
        @else
            <x-button title="{{ $button->title }}"
                url="{{ $button->page ? get_permalink($button->page) : $button->section_id }}"
                color="{{ $button->color }}">
                {{ $button->title }}
            </x-button>
        @endif
    @endforeach
</x-buttons>
