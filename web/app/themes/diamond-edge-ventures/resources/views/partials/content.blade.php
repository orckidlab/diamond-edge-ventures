<article class="prose lg:prose-xl" @php(post_class())>
    <header>
        <h3 class="entry-title">
            <a href="{{ get_permalink() }}">
                {!! $title !!}
            </a>
        </h3>
        @include('partials.entry-meta')
    </header>
    <div class="entry-summary">
        @php(the_excerpt())
    </div>
</article>
