@php
    $postCategories = wp_get_post_terms($post->ID, 'category');
    $postCategories = wp_list_filter($postCategories, ['slug' => 'all'], 'NOT');
    $postTags = wp_get_post_terms($post->ID, 'post_tag');
    $category = $postCategories[0];
    $icon = get_field('svg_icon', $category->taxonomy . '_' . $category->term_id);
    $color = get_field('tailwind_class', $category->taxonomy . '_' . $category->term_id);
    $title = mb_strimwidth(get_the_title(), 0, 70, '...');
    $subtitle = mb_strimwidth(get_field('subtitle'), 0, 70, '...');
    $excerpt = mb_strimwidth(get_the_excerpt(), 0, 180, '...');
@endphp

<a class="card group max-xl:!h-auto bg-{{ $color ?? 'amber-400' }} max-sm:shrink-0 relative col-span-1 flex h-full w-full flex-col overflow-hidden after:block"
    href="{{ get_permalink() }}">
    <div class="flex flex-col p-6">
        <figure class="not-prose !mb-0 !ml-auto">
            <picture class="flex w-16 content-center justify-center bg-contain bg-center bg-no-repeat">
                @if ($icon)
                    <img class="!h-auto !w-6/12 !object-contain" src="{{ $icon }}" alt="{{ $category->name }}">
                @endif
            </picture>
        </figure>
        <h3 class="default mb-4 text-sm text-black after:!content-none lg:text-base">
            {{ $category->name }}
        </h3>
        <h4 class="mb-2 truncate text-xl !font-bold lg:text-2xl">{{ $title }}</h4>
        <h5 class="mb-4 truncate text-base !font-bold after:!content-none">{{ $subtitle }}</h5>
        <ul
            class="not-prose flex flex-wrap space-x-2 divide-x divide-black text-xs font-medium !leading-none text-black lg:text-base">
            @if ($postTags)
                @foreach ($postTags as $tag)
                    <li class="[&:not(:first-child)]:pl-2">{{ $tag->name }}</li>
                @endforeach
            @endif
            <li class="pl-2">
                <time class="updated" itemprop="datePublished"
                    datetime="{{ get_post_time('c', true) }}">{{ get_the_date() }}</time>
            </li>
        </ul>
    </div>
    <div
        class="card-child bg-dark-700 left-0 bottom-0 z-20 mt-auto flex w-full flex-col p-6 transition-all ease-in-out xl:absolute xl:group-hover:pt-[14%]">
        <p
            class="max-sm:truncate text-sm transition-all ease-in-out md:text-base xl:group-hover:-translate-y-9 xl:group-hover:text-teal-900">
            {{ $excerpt }}
        </p>
        <button class="button primary mt-6 xl:opacity-0 xl:group-hover:-translate-y-9 xl:group-hover:opacity-100"
            type="button">
            Read more
        </button>
    </div>
</a>
</article>
