<a class="card group relative col-span-1 flex h-full w-full flex-col overflow-hidden after:block max-xl:!h-auto max-sm:shrink-0"
    href="{{ $postUrl ?? '#' }}" title="{{ $postTitle }}" target="_blank">
    <x-article>
        <figure class="mb-6 flex h-72 w-full content-center items-center justify-center">
            <img class="h-full w-full object-cover" src="{{ $postImage }}" alt="{{ $postTitle }}" loading="lazy">
        </figure>
        <h4 class="font-bold">{{ $postTitle }}</h4>
    </x-article>
    <x-buttons class="justify-center lg:mt-6">
        <x-button class="!m-auto w-fit" tag="button">
            Read more
        </x-button>
    </x-buttons>
</a>
