@php
    $postCategories = wp_get_post_terms($post->ID, 'category');
    $postCategories = wp_list_filter($postCategories, ['slug' => 'all'], 'NOT');
    $postTags = wp_get_post_terms($post->ID, 'post_tag');
    $category = $postCategories[0];
@endphp

@include('partials.post', [
    'postUrl' => get_permalink(),
    'postTitle' => mb_strimwidth(get_the_title(), 0, 70, '...'),
    'postSubtitle' => mb_strimwidth(get_field('subtitle'), 0, 70, '...'),
    'postDate' => get_the_date(),
    'postColor' => get_field('tailwind_class', $category->taxonomy . '_' . $category->term_id),
    'postIcon' => get_field('svg_icon', $category->taxonomy . '_' . $category->term_id),
    'postCategoryName' => $category->name,
    'postTags' => $postTags,
    'postExcerpt' => mb_strimwidth(get_the_excerpt(), 0, 180, '...'),
])
