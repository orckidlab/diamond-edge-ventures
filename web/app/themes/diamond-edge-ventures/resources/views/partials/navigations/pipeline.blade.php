<ul
    class="child:w-full child:md:w-auto flex flex-row flex-wrap items-center space-y-5 divide-amber-400 font-medium md:space-x-3 md:space-y-0 md:divide-x md:leading-none [&>*:nth-child(n+1)]:max-sm:mr-5 [&>*:nth-child(n+3)]:w-auto">
    @foreach ($links as $link)
        <li class="{{ $link->classes ?? '' }} {{ $link->active ? 'active' : '' }} [&:not(:first-child)]:md:pl-3">
            <a class="child-svg:w-5 md:child-svg:w-4 child-svg:fill-current hover:text-amber-400"
                href="{{ $link->url }}" title="{{ $link->label }}">
                {!! $link->label !!}
            </a>
        </li>
    @endforeach
</ul>
