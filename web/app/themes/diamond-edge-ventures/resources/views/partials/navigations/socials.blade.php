<ul class="flex space-x-8">
    @foreach ($links as $link)
        <li>
            <a class="transition-opacity hover:opacity-50" href="{{ $link->url }}" title="{{ $link->title }}"
                target="{{ $link->target }}">
                {!! $link->label !!}
            </a>
        </li>
    @endforeach
</ul>
