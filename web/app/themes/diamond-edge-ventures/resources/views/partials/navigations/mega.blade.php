<ul class="primary-navigation ml-auto hidden space-x-12 lg:flex">
    @foreach ($links as $item)
        <li
            class="menu-item {{ $item->children ? 'has-children' : '' }} {{ $item->classes ?? '' }} {{ $item->active || $item->activeAncestor || $item->activeParent ? 'child-a:after:!bg-blue-800' : '' }} relative">
            <a class="whitespace-no-wrap {{ $item->children ? 'cursor-default' : '' }} group peer relative inline-flex items-center py-4 after:absolute after:bottom-2 after:left-0 after:h-1 after:w-full after:bg-transparent after:transition-all hover:after:bg-blue-800"
                href="{{ $item->url }}" title="{{ $item->label }}" target="{{ $item->target }}">
                @php
                    $icon = get_field('icon', $item->id);
                @endphp
                @if ($icon)
                    @svg($icon, 'absolute w-6 h-6 -left-7', ['aria-label' => 'icon'])
                @endif
                <span>{{ $item->label }}</span>
                @if ($item->children)
                    @svg('images.icons.x-chevron-right', 'w-4 h-4 rotate-90 ml-2 transition-all absolute -right-6 group-hover:-rotate-90', ['aria-label' => 'more links'])
                @endif
            </a>
            @if ($item->children)
                <menu
                    class="drop-shadow-3xl pointer-events-none absolute -left-4 top-[86px] z-10 grid min-w-max bg-white text-sm opacity-0 transition-all before:absolute before:-top-8 before:left-0 before:h-10 before:w-full before:bg-transparent hover:pointer-events-auto hover:opacity-100 peer-hover:pointer-events-auto peer-hover:opacity-100">
                    <ul class="children">
                        @foreach ($item->children as $child)
                            <li
                                class="{{ $child->children ? 'has-more-children' : '' }} {{ $child->classes ?? '' }} {{ $child->active ? 'active' : '' }} z-20 w-full cursor-pointer bg-white text-lg">
                                <a class="{{ $child->children ? 'w-96' : 'min-w-max' }} peer flex items-center justify-between whitespace-nowrap px-6 py-3 text-base hover:text-red-900"
                                    href="{{ $child->url }}" title="{!! $child->label !!}"
                                    target="{{ $child->target }}">
                                    <span>{!! $child->label !!}</span>
                                    @if ($child->children)
                                        @svg('images.icons.x-arrow', 'w-6 h-6', ['aria-label' => 'more links'])
                                    @endif
                                </a>
                                @if ($child->children)
                                    <menu
                                        class="absolute right-0 top-0 -z-10 hidden h-full w-6/12 bg-amber-400 transition-all hover:flex peer-hover:flex">
                                        <ul class="child h-full w-full">
                                            @foreach ($child->children as $nestedChild)
                                                <li
                                                    class="menu-item {{ $nestedChild->classes ?? '' }} {{ $child->active ? 'active' : '' }} w-full justify-start">
                                                    <a class="block whitespace-nowrap px-8 py-5 hover:bg-amber-400"
                                                        href="{{ $nestedChild->url }}"
                                                        title=" {{ $nestedChild->label }}">
                                                        {{ $nestedChild->label }}
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </menu>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                </menu>
            @endif
        </li>
    @endforeach
</ul>
