<ul class="space-y-4">
    @foreach ($links as $link)
        <li>
            <a class="transition-colors hover:text-blue-200" href="{{ $link->url }}" title="{{ $link->label }}"
                target="{{ $link->target }}">
                {!! $link->label !!}
            </a>
        </li>
    @endforeach
</ul>
