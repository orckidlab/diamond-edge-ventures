<a class="drop-shadow-3xl fixed top-1/2 -right-1 z-50 hidden translate-x-14 -rotate-90 content-center items-center justify-center overflow-hidden rounded-md border-2 border-blue-700 bg-blue-700 py-2 px-4 text-xl font-bold text-white transition-colors hover:border-blue-900 hover:bg-blue-900 sm:px-7 md:inline-flex md:py-3 2xl:text-2xl"
    href="/contact-us" title="Contact us">
    Contact us
</a>
