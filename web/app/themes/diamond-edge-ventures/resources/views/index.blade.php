@extends('layouts.app')

@section('content')
    @include('partials.page-header')
    <x-section class="!pt-0">
        <x-article>
            <h4>Select your topic of interest:</h4>
        </x-article>
        @php
            $tags = get_tags();
        @endphp
        @if ($tags)
            <x-buttons class="max-sm:px-8">
                @foreach (get_tags() as $key => $tag)
                    <x-button url="{{ get_term_link($tag) }}" color="outline-dark">
                        {{ $tag->name }}
                    </x-button>
                @endforeach
            </x-buttons>
        @endif

        @if (!have_posts())
            <x-alert type="default">
                {!! __('Sorry, no results were found.', 'sage') !!}
            </x-alert>
            {!! get_search_form(false) !!}
        @endif

        <x-article class="text-white">
            <x-grid
                class="not-prose !mt-12 grid-cols-1 gap-6 gap-y-6 max-sm:relative max-sm:flex max-sm:w-full max-sm:snap-x max-sm:snap-mandatory max-sm:overflow-x-auto max-sm:px-8 md:grid-cols-2 lg:grid-cols-3">
                @while (have_posts())
                    @php(the_post())
                    @includeFirst(['partials.content-' . get_post_type(), 'partials.content'])
                @endwhile
            </x-grid>
        </x-article>

        <footer class="heir-h2:hidden heir-a:hover:text-amber-400 mt-6">
            {!! the_posts_navigation([
                'prev_text' => '← Prev',
                'next_text' => 'Next →',
                'screen_reader_text' => 'Posts Navigation',
            ]) !!}
        </footer>

    </x-section>
@endsection
