{{--
  Template Name: Default Page
--}}

@extends('layouts.app')

@section('content')
    @if ($hero->type === 'breadcrumb-only')
        <x-section class="!py-4">
            <x-breadcrumb></x-breadcrumb>
        </x-section>
    @else
        <section
            class="{{ $hero->background }} relative flex min-h-[340px] w-full flex-col justify-center overflow-hidden xl:px-8"
            role="banner" aria-label="hero">
            <menu class="absolute left-0 top-4 z-20 w-full">
                <main class="container">
                    <x-breadcrumb></x-breadcrumb>
                </main>
            </menu>
            <main class="container relative z-10 order-last">
                <x-article class="w-full py-8 max-sm:px-8 lg:w-5/12 xl:w-6/12 xl:py-16">
                    <h1>{!! get_the_title() !!}</h1>
                </x-article>
            </main>
            <figure class="right-0 h-full w-full mix-blend-multiply lg:absolute lg:w-7/12 xl:w-6/12">
                <img class="h-full w-full object-cover" src="{{ $hero->image }}" alt="{{ get_the_title() }}">
            </figure>
        </section>
    @endif
    <x-section class="bg-blue-300">
        <x-article class="m-auto !max-w-7xl">
            @php(the_content())
        </x-article>
    </x-section>
@endsection
