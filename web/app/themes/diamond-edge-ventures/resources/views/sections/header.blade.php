<header class="drop-shadow-3xl sticky top-0 z-40 w-full bg-white py-4 px-8" id="main-header">
    <main class="container">
        <nav class="flex flex-wrap items-center justify-between">
            <a class="mr-12 flex items-center space-x-5 divide-x divide-black" href="{{ home_url('/') }}"
                title="{!! $website !!}">
                @svg('images.brand.logo-diamond', 'w-full max-w-[200px] lg:max-w-[350px] h-auto', ['aria-label' => $website])
            </a>
            @if ($primaryNavigation)
                @include('partials.navigations.mega', ['links' => $primaryNavigation])
            @endif
            <ul class="ml-8 flex flex-wrap items-center space-x-1 lg:space-x-12">
                <li class="block xl:hidden">
                    <button class="button icon-only -mr-2.5 web-nav-close" type="button" @click="toggleMobileNav">
                        @svg('images.icons.x-menu', 'w-6 h-6', ['aria-label' => 'Open navigation'])
                    </button>
                </li>
            </ul>
        </nav>
    </main>
</header>
