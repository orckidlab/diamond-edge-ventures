<section class="scroll-mt-20" id="dynamic-sections">

    {{-- Loop to construct layout accordingly --}}
    @foreach ($sections as $key => $section)

        @php
            $layout = (object) $section;
            $type = $layout->acf_fc_layout;
        @endphp

        {{-- {{ dd($layout) }} --}}

        {{-- Embed Layout --}}
        @if ($type == 'embed_layout')
            @if ($layout->type == 'blogs')
                @include('sections.blogs', [
                    'posts' => $postsByCategory(3, 'articles'),
                ])
            @else
                @include("sections.$layout->template_type")
            @endif
        @endif

        {{-- Generic Content --}}
        @if ($type == 'generic_content')
            <x-section
                class="{{ $layout->default_background_color }} {{ $layout->default_remove_top_padding ? '!pt-0' : '' }}"
                id="section-{{ $key }}">
                @if ($layout->headline_optional)
                    <x-article class="mb-8">
                        <h3>{{ $layout->headline_optional }}</h3>
                    </x-article>
                @endif
                <x-grid class="gap-10 md:grid-cols-2 md:gap-20">
                    @if ($layout->primary_content)
                        <x-article class="{{ $layout->reverse_flow ? 'order-1' : '' }} heir-img:!m-0 max-sm:order-1">
                            {!! $layout->primary_content !!}
                            @if ($layout->default_buttons)
                                @include('partials.buttons', ['buttons' => $layout->default_buttons])
                            @endif
                        </x-article>
                    @endif
                    @if ($layout->secondary_content)
                        <x-article class="heir-img:!m-0">
                            {!! $layout->secondary_content !!}
                        </x-article>
                    @endif
                    @if ($layout->video && !$layout->secondary_content)
                        <x-article>
                            <video class="!m-0 aspect-video" src="{{ $layout->video }}" type="video/mp4" autoplay muted
                                loop playsinline></video>
                        </x-article>
                    @endif
                </x-grid>
            </x-section>
        @endif

        {{-- Content with video --}}
        @if ($type == 'content_with_video')
            <x-section class="bg-blue-700" id="section-{{ $key }}">
                <x-grid class="gap-10 md:grid-cols-2 md:gap-20">
                    <x-article class="prose-invert text-white">
                        {!! $layout->video_content !!}
                        @if ($layout->default_buttons)
                            @include('partials.buttons', [
                                'buttons' => $layout->default_buttons,
                            ])
                        @endif
                    </x-article>
                    <x-article class="prose-invert text-white">
                        <div class="mb-4">
                            {!! $layout->video !!}
                        </div>
                        {!! $layout->video_description !!}
                    </x-article>
                </x-grid>
            </x-section>
        @endif

        {{-- Pages with icons --}}
        @if ($type == 'pages_with_icons')
            <x-section class="{{ $layout->remove_top_padding ? '!pt-0' : '' }} bg-blue-300"
                id="section-{{ $key }}">
                <x-article class="m-auto mb-8 max-w-7xl text-center">
                    {!! $layout->description !!}
                </x-article>
                <x-grid class="grid-cols-2 gap-2 md:grid-cols-4 md:gap-10">
                    @foreach ($layout->related_pages as $relatedPage)
                        {{-- {{ dd($layout->related_pages) }} --}}
                        @php
                            $title = get_the_title($relatedPage);
                            $icon = get_field('icon_default_state', $relatedPage);
                            $iconHover = get_field('icon_hover_state', $relatedPage);
                            $url = get_permalink($relatedPage);
                        @endphp
                        <a class="group block text-center no-underline" href="{{ $url }}"
                            title="{{ $title }}">
                            <figure class="inline-flex h-full flex-col justify-center space-x-2 md:space-x-0">
                                <picture
                                    class="relative m-auto inline-flex h-36 w-32 flex-col content-center items-center justify-center text-white">
                                    @svg($icon, 'w-36 w-full object-cover absolute transition-opacity', ['aria-label' => $title])
                                    @svg($iconHover, 'absolute w-full object-cover opacity-0 transition-opacity group-hover:opacity-100', ['aria-label' => $title])
                                </picture>
                                <figcaption class="!m-0">
                                    <h4 class="!m-0">{!! $title !!}</h4>
                                </figcaption>
                            </figure>
                        </a>
                    @endforeach
                </x-grid>
            </x-section>
        @endif

        {{-- Highlight block --}}
        @if ($type == 'highlight_block')
            <section class="relative flex w-full flex-col justify-center overflow-hidden bg-blue-500"
                id="section-{{ $key }}" role="doc-chapter">
                <figure class="left-0 h-full w-full mix-blend-multiply lg:absolute lg:w-4/12 xl:w-4/12">
                    <img class="h-full w-full object-cover" src="{{ $layout->highlighted_image }}"
                        alt="{{ $layout->cta_label }}" loading="lazy">
                </figure>
                <main class="container relative z-10 order-last flex justify-end">
                    <x-article class="w-full py-8 max-sm:px-8 lg:w-5/12 xl:w-6/12 xl:py-16">
                        <p> {{ $layout->highlighted_content }}</p>
                        <x-buttons>
                            <x-button title="{{ $layout->cta_label }}" url="{{ $layout->cta_url }}">
                                {{ $layout->cta_label }}</x-button>
                        </x-buttons>
                    </x-article>
                </main>
            </section>
        @endif

        {{-- Content with image as half background --}}
        @if ($type == 'content_with_image_as_background')
            <x-section class="relative bg-blue-700" id="section-{{ $key }}">
                @if (wp_is_mobile())
                    <figure class="mb-8 w-full">
                        <img class="h-full w-full object-cover" src="{{ $layout->image }}"
                            alt="{{ strip_tags($layout->content) }}" loading="lazy">
                    </figure>
                @else
                    <figure class="pointer-events-none absolute top-0 bottom-0 right-0 z-10 h-full w-6/12">
                        <picture class="flex h-full w-full bg-cover bg-right-top bg-no-repeat"
                            style="background-image: url('{{ $layout->image }}')">
                        </picture>
                    </figure>
                @endif
                <x-grid class="min-h-0 gap-10 md:min-h-[500px] md:grid-cols-2 md:gap-20">
                    <x-article class="prose-invert flex flex-col justify-center text-white">
                        {!! $layout->content !!}
                        @if ($layout->default_buttons)
                            @include('partials.buttons', ['buttons' => $layout->default_buttons])
                        @endif
                    </x-article>
                </x-grid>
            </x-section>
        @endif

        {{-- Content with stats --}}
        @if ($type == 'content_with_stats')
            <x-section id="section-{{ $key }}">
                <x-article class="mb-6 max-w-7xl">
                    {!! $layout->content !!}
                </x-article>
                <x-grid class="grid-cols-1 gap-1 md:grid-cols-3 md:gap-10">
                    @foreach ($layout->stats as $stat)
                        @php($stat = (object) $stat)
                        <x-article>
                            <figure
                                class="inline-flex h-full flex-row content-center items-center justify-center space-x-2 md:space-x-0">
                                <picture
                                    class="m-auto inline-flex h-36 w-32 flex-col content-center items-center justify-center">
                                    @svg($stat->icon, 'w-full object-cover', ['aria-label' => $stat->title])
                                </picture>
                                <figcaption class="!m-0">
                                    <h4 class="!m-0 text-blue-700">{{ $stat->title }}</h4>
                                    @if ($stat->description)
                                        <small>{{ $stat->description }}</small>
                                    @endif
                                </figcaption>
                            </figure>
                        </x-article>
                    @endforeach
                </x-grid>
            </x-section>
        @endif

        {{-- Content with video (centered) --}}
        @if ($type == 'content_with_video_centered')
            <x-section class="{{ $layout->background_color }} {{ $layout->remove_top_padding ? '!pt-0' : '' }}"
                id="section-{{ $key }}">
                @if ($layout->background_color === 'bg-blue-700' || $layout->background_color === 'bg-split-blue-white-3')
                    <x-article class="prose-invert m-auto mb-6 max-w-7xl text-center text-white">
                        {!! $layout->content !!}
                        <figure class="m-auto w-6/12">
                            {!! $layout->video_link !!}
                        </figure>
                    </x-article>
                @else
                    <x-article class="m-auto mb-6 max-w-7xl text-center">
                        {!! $layout->content !!}
                        <figure class="m-auto w-full md:w-6/12">
                            {!! $layout->video_link !!}
                        </figure>
                    </x-article>
                @endif
            </x-section>
        @endif

        {{-- Grid --}}
        @if ($type == 'grids_layout')
            <x-section class="{{ $layout->remove_top_padding ? '!pt-0' : '' }}" id="section-{{ $key }}">
                <x-grid class="gap-10 md:grid-cols-2 md:gap-10">
                    @foreach ($layout->grids as $grid)
                        @php($grid = (object) $grid)
                        <x-article class="rounded-md bg-blue-300 p-8">
                            {!! $grid->content !!}
                            @if ($grid->default_buttons)
                                @include('partials.buttons', ['buttons' => $grid->default_buttons])
                            @endif
                        </x-article>
                    @endforeach
                </x-grid>
            </x-section>
        @endif

        {{-- Accordion --}}
        @if ($type == 'accordions')
            <x-section
                class="{{ $layout->accordions_remove_top_padding ? '!pt-0' : '' }} {{ $layout->background_color }}"
                id="section-{{ $key }}">
                <x-article class="m-auto mb-6 max-w-7xl">
                    {!! $layout->content !!}
                    @foreach ($layout->accordions as $accordion)
                        @php($accordion = (object) $accordion)
                        <app-accordion>
                            <template v-slot:title>
                                <h4 class="!m-0 text-inherit">{{ $accordion->title }}</h4>
                            </template>
                            <template v-slot:content>
                                <x-grid class="my-10 gap-10 md:grid-cols-2 md:gap-20">
                                    @if ($accordion->primary_column)
                                        <x-article class="heir-img:!m-0">
                                            {!! $accordion->primary_column !!}
                                            @if ($accordion->default_buttons)
                                                @include('partials.buttons', [
                                                    'buttons' => $accordion->default_buttons,
                                                ])
                                            @endif
                                        </x-article>
                                    @endif
                                    @if ($accordion->secondary_column)
                                        <x-article class="heir-img:!m-0">
                                            {!! $accordion->secondary_column !!}
                                        </x-article>
                                    @endif
                                </x-grid>
                            </template>
                        </app-accordion>
                    @endforeach
                    @if ($layout->default_buttons)
                        @include('partials.buttons', ['buttons' => $layout->default_buttons])
                    @endif
                </x-article>
            </x-section>
        @endif

        {{-- Singular Content --}}
        @if ($type == 'singular_content')
            <x-section class="{{ $layout->background_color }}" id="section-{{ $key }}">
                @if ($layout->background_color === 'bg-blue-700' || $layout->background_color === 'bg-split-blue-white-3')
                    <x-article class="prose-invert m-auto !max-w-7xl text-white">
                        {!! $layout->content !!}
                        @if ($layout->cta_url)
                            <x-buttons class="w-full md:w-auto">
                                <x-button url="{{ $layout->cta_url }}" color="white">
                                    {{ $layout->singular_cta_label }}
                                </x-button>
                            </x-buttons>
                        @endif
                        @if ($layout->image_after_cta)
                            <figure class="mb-8 w-6/12">
                                <img class="h-full w-full object-cover" src="{{ $layout->image_after_cta }}"
                                    alt="image" loading="lazy">
                            </figure>
                        @endif
                        @if ($layout->video)
                            <figure class="w-full md:w-6/12">
                                {!! $layout->video !!}
                            </figure>
                        @endif
                    </x-article>
                @else
                    <x-article class="m-auto !max-w-7xl">
                        {!! $layout->content !!}
                        @if ($layout->cta_url)
                            <x-buttons class="w-full md:w-auto">
                                <x-button url="{{ $layout->cta_url }}" color="white">
                                    {{ $layout->singular_cta_label }}
                                </x-button>
                            </x-buttons>
                        @endif
                        @if ($layout->image_after_cta)
                            <figure class="mb-8 w-full">
                                <img class="h-full w-full object-cover" src="{{ $layout->image_after_cta }}"
                                    alt="image" loading="lazy">
                            </figure>
                        @endif
                        @if ($layout->video)
                            <figure class="w-full md:w-6/12">
                                {!! $layout->video !!}
                            </figure>
                        @endif
                    </x-article>
                @endif
            </x-section>
        @endif

        {{-- Tabs --}}
        @if ($type == 'tabs_layout')
            <x-section class="{{ $layout->background_color }} !pb-0" id="section-{{ $key }}">
                <x-article class="m-auto mb-6 max-w-7xl text-center">
                    <h3>{{ $layout->title }}</h3>
                </x-article>
                <article class="mt-12">
                    <ul
                        class="divide-grey-400 grid auto-cols-max grid-flow-col divide-x max-sm:flex max-sm:w-full max-sm:snap-x max-sm:snap-mandatory max-sm:overflow-x-auto md:justify-center">
                        @foreach ($layout->tabs as $tab)
                            @php($tab = (object) $tab)
                            <li class="max-sm:shrink-0">
                                <a class="text-grey-300 bg-grey-100 relative inline-block w-full cursor-pointer px-8 py-4 text-center transition-all before:absolute before:top-0 before:left-0 before:z-20 before:h-1 before:w-full before:bg-transparent hover:opacity-75"
                                    href="#step-{{ $loop->index + 1 }}" title="{{ $tab->tab }}"
                                    :class="{
                                        'font-semibold before:!bg-blue-600 !bg-white': tab ===
                                            {{ $loop->index + 1 }}
                                    }"
                                    @click="currentTab({{ $loop->index + 1 }})">
                                    {{ $tab->tab }}
                                </a>
                            </li>
                        @endforeach
                    </ul>
                </article>
            </x-section>
            @foreach ($layout->tabs as $tab)
                @php($tab = (object) $tab)
                <section class="{{ $layout->background_color }} scroll-mt-52" id="step-{{ $loop->index + 1 }}"
                    v-if="tab === {{ $loop->index + 1 }}">
                    <x-section class="!pt-0">
                        <div class="drop-shadow-3xl space-y-8 bg-white p-8">
                            @foreach ($tab->content as $tabContent)
                                @php($tabContent = (object) $tabContent)

                                @if ($tabContent->acf_fc_layout == 'generic_content_two_columns')
                                    <div>
                                        @if ($tabContent->headline_optional)
                                            <x-article class="mb-8">
                                                <h3>{{ $tabContent->headline_optional }}</h3>
                                            </x-article>
                                        @endif
                                        <x-grid class="gap-10 md:grid-cols-2 md:gap-20">
                                            @if ($tabContent->primary_content)
                                                <x-article
                                                    class="{{ $tabContent->reverse_flow ? 'order-1' : '' }} heir-img:!m-0 child-table:bg-white">
                                                    {!! $tabContent->primary_content !!}
                                                    @if ($tabContent->default_buttons_conditional)
                                                        @include('partials.buttons', [
                                                            'buttons' => $tabContent->default_buttons,
                                                        ])
                                                    @endif
                                                </x-article>
                                            @endif
                                            @if ($tabContent->secondary_content)
                                                <x-article class="heir-img:!m-0">
                                                    {!! $tabContent->secondary_content !!}
                                                </x-article>
                                            @endif
                                        </x-grid>
                                    </div>
                                @endif

                                @if ($tabContent->acf_fc_layout == 'icon_list')
                                    <div>
                                        <x-grid class="grid-cols-1 gap-2 md:grid-cols-3 md:gap-20">
                                            @foreach ($tabContent->icons as $item)
                                                @php($item = (object) $item)
                                                <x-article>
                                                    <figure class="!m-0 inline-flex flex-col space-x-2 md:space-x-0">
                                                        <picture
                                                            class="relative inline-flex w-32 flex-col content-center items-center">
                                                            @svg($item->icon, 'w-36 w-full object-cover', ['aria-label' => 'icon'])
                                                        </picture>
                                                        <figcaption class="!m-0">
                                                            {!! $item->content !!}
                                                        </figcaption>
                                                    </figure>
                                                    <x-buttons class="w-full md:w-auto lg:mt-0">
                                                        <x-button class="group !p-0 hover:text-blue-900"
                                                            url="{{ get_permalink($item->page) }}"
                                                            color="transparent" tag="a">
                                                            <span
                                                                class="mr-2 transition-all group-hover:mr-3">{{ $item->cta_label }}</span>
                                                            @svg('images.icons.x-chevron-right', '!w-5 !h-5', ['aria-label' => 'View'])
                                                        </x-button>
                                                    </x-buttons>
                                                </x-article>
                                            @endforeach
                                        </x-grid>
                                    </div>
                                @endif

                                @if ($tabContent->acf_fc_layout == 'related_articles')
                                    <div>
                                        <x-article>
                                            <h3>Related Articles</h3>
                                            <wyxos-glider class="thumbnails mt-8" :config="$GLIDER_PRESETS.THUMBNAILS">
                                                @foreach ($tabContent->articles as $article)
                                                    <a class="not-prose group flex items-center justify-center"
                                                        href="{{ get_field('url', $article->ID) }}"
                                                        title="{{ $article->post_title }}" target="_blank">
                                                        <figure
                                                            class="after:bg-overlay relative h-full w-full overflow-hidden after:absolute after:top-0 after:left-0 after:h-full after:w-full">
                                                            <img class="object-fit h-full w-full transition-all group-hover:scale-110"
                                                                src="{{ $thumbnailURL($article->ID, 'large') }}"
                                                                alt="{{ $article->post_title }}">
                                                            <figcaption class="absolute bottom-0 left-0 z-10 p-6">
                                                                <h5 class="text-base font-semibold text-white">
                                                                    {{ mb_strimwidth($article->post_title, 0, 32, '...') }}
                                                                </h5>
                                                            </figcaption>
                                                        </figure>
                                                    </a>
                                                @endforeach
                                            </wyxos-glider>
                                        </x-article>
                                    </div>
                                @endif
                            @endforeach
                        </div>

                    </x-section>

                </section>
            @endforeach
        @endif

        {{-- Steps content --}}
        @if ($type == 'steps_content')
            <x-section class="bg-blue-300" id="section-{{ $key }}">
                <x-grid class="grid-cols-1 gap-1 md:grid-cols-3 md:gap-20">
                    @foreach ($layout->steps as $step)
                        @php($step = (object) $step)
                        <x-article class="child-h4:!text-blue-700">
                            {!! $step->step !!}
                        </x-article>
                    @endforeach
                </x-grid>
            </x-section>
        @endif

        {{-- Content with 3 columns (mixed) --}}
        @if ($type == 'content_with_3_columns_mixes')
            <x-section class="bg-blue-300 !pt-0" id="section-{{ $key }}">
                <x-article class="heir-img:!mt-12 mb-12 text-center">
                    {!! $layout->primary_column !!}
                </x-article>
                <x-grid class="gap-10 md:grid-cols-2 md:gap-20">
                    @if ($layout->secondary_column)
                        <x-article class="heir-img:!m-0">
                            {!! $layout->secondary_column !!}
                        </x-article>
                    @endif
                    @if ($layout->tertiary_column)
                        <x-article class="heir-img:!m-0">
                            {!! $layout->tertiary_column !!}
                        </x-article>
                    @endif
                </x-grid>
            </x-section>
        @endif

        {{-- Icons only with tooltips --}}
        @if ($type == 'icons_with_tooltips')
            <x-section class="bg-blue-300 !pt-0" id="section-{{ $key }}">
                <x-grid class="grid-cols-2 gap-2 md:grid-cols-7 md:gap-10">
                    @foreach ($layout->icons as $icon)
                        @php($icon = (object) $icon)
                        <x-article class="text-center text-white">
                            <a class="group relative block cursor-default" href="javascript:"
                                title="{{ $icon->title }}">
                                <div
                                    class="rotate-y-0 group-hover:rotate-y-180 block border bg-white transition-all duration-200 will-change-transform">
                                    <figure
                                        class="!m-0 inline-flex h-full flex-col justify-center space-x-2 py-8 md:space-x-0">
                                        <picture
                                            class="relative m-auto inline-flex h-36 w-32 flex-col content-center items-center justify-center text-white">
                                            @svg($icon->icon_default_state, 'w-36 w-full object-cover absolute transition-opacity', ['aria-label' => $icon->title])
                                            {{-- @svg($icon->icon_hover_state, 'w-36 w-full object-cover absolute transition-opacity opacity-0 group-hover:opacity-100', ['aria-label' => $icon->title]) --}}
                                        </picture>
                                        <figcaption class="!m-0">
                                            <h4 class="!m-0">{!! $icon->title !!}</h4>
                                        </figcaption>
                                    </figure>
                                </div>
                                <div
                                    class="rotate-negative-y-180 group-hover:rotate-y-0 absolute top-0 left-0 flex h-full w-full content-center items-center justify-center border bg-blue-700 p-4 text-center text-white opacity-0 transition-all duration-200 will-change-transform group-hover:opacity-100">
                                    <small>{{ $icon->description }}</small>
                                </div>
                            </a>
                        </x-article>
                    @endforeach
                </x-grid>
            </x-section>
        @endif

        {{-- Articles --}}
        @if ($type == 'related_articles')
            <x-section>
                <x-article>
                    <h3>Related Articles</h3>
                    <wyxos-glider class="thumbnails mt-8" :config="$GLIDER_PRESETS.THUMBNAILS">
                        @foreach ($layout->articles as $article)
                            <a class="not-prose group flex items-center justify-center"
                                href="{{ get_field('url', $article->ID) }}" title="{{ $article->post_title }}"
                                target="_blank">
                                <figure
                                    class="after:bg-overlay relative h-full w-full overflow-hidden after:absolute after:top-0 after:left-0 after:h-full after:w-full">
                                    <img class="object-fit h-full w-full transition-all group-hover:scale-110"
                                        src="{{ $thumbnailURL($article->ID, 'large') }}"
                                        alt="{{ $article->post_title }}">
                                    <figcaption class="absolute bottom-0 left-0 z-10 p-6">
                                        <h5 class="text-base font-semibold text-white">
                                            {{ mb_strimwidth($article->post_title, 0, 32, '...') }}
                                        </h5>
                                    </figcaption>
                                </figure>
                            </a>
                        @endforeach
                    </wyxos-glider>
                </x-article>
            </x-section>
        @endif

    @endforeach
</section>
