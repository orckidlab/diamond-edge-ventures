<x-section class="bg-blue-300 !pt-0 max-sm:px-0">
    <hr
        class="before:bg-hr relative mb-20 h-px border-none before:absolute before:top-0 before:right-[5%] before:left-[5%] before:h-px before:w-11/12 before:content-['']">
    <x-article class="text-center">
        <h2 class="!text-sm !font-normal uppercase text-blue-700 max-sm:px-8">Resources & Articles</h2>
        <h3 class="max-sm:px-8">{{ $blogTitle ?? 'Most Recent Articles' }}</h3>
        @if ($posts)
            <x-grid
                class="not-prose !mt-12 grid-cols-1 gap-6 gap-y-6 max-sm:relative max-sm:flex max-sm:w-full max-sm:snap-x max-sm:snap-mandatory max-sm:overflow-x-auto max-sm:px-8 md:grid-cols-2 lg:grid-cols-3">
                @foreach ($posts as $post)
                    @include('partials.post', [
                        'postUrl' => $post->url,
                        'postTitle' => $post->title,
                        'postImage' => $post->thumbnail,
                    ])
                @endforeach
            </x-grid>
        @else
            <x-alert type="default">
                {!! __('No related blog available in the category selected!', 'sage') !!}
            </x-alert>
        @endif
    </x-article>
    <x-buttons class="justify-center lg:mt-12">
        <x-button class="!m-auto w-fit hover:text-blue-900" color="transparent" url="/articles-and-resources"
            tag="a">
            <span class="mr-2">View all</span>
            @svg('images.icons.x-chevron-right', '!w-5 !h-5', ['aria-label' => 'View all'])
        </x-button>
    </x-buttons>
</x-section>
