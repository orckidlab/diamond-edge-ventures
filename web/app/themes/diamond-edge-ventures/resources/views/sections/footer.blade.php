<footer class="sticky top-[100vh] bg-white" id="main-footer">
    <x-section class="!pb-4">
        <article class="mb-16 justify-between gap-4 flex flex-col md:flex-row">
            <nav>
                <figure class="mb-12">
                    <a href="{{ home_url('/') }}" title="{!! $website !!}">
                        @svg('images.brand.footer-logo', 'w-full md:w-auto', ['aria-label' => $website])
                    </a>
                </figure>
                @if ($socialsNavigation)
                    @include('partials.navigations.socials', ['links' => $socialsNavigation])
                @endif
            </nav>
            <div class="grid-cols-2 gap-4 md:grid">
                @if ($footerNavigation)
                    <nav class="my-8 md:m-0">
                        <header class="prose mb-8">
                            <h4>Quick Links</h4>
                        </header>
                        @include('partials.navigations.default', ['links' => $footerNavigation])
                    </nav>
                @endif
                @if ($legalsNavigation)
                    <nav class="my-8 md:m-0">
                        <header class="prose mb-8">
                            <h4>Help & Support</h4>
                        </header>
                        @include('partials.navigations.default', ['links' => $legalsNavigation])
                    </nav>
                @endif
            </div>
        </article>
        <nav class="flex w-full">
            <a class="group my-6 inline-flex content-center items-center justify-center space-x-4 text-blue-800 md:ml-auto"
                href="#main-content" title="Back to top">
                <span
                    class="inline-flex h-8 w-8 origin-center content-center items-center justify-center rounded-full bg-blue-800 text-white transition-all group-hover:h-14 group-hover:w-14 group-hover:bg-blue-900 md:h-12 md:w-12">
                    @svg('images.icons.x-arrow-long-up', 'h-6 w-6 md:h-8 md:w-8', ['aria-label' => 'Go top'])
                </span>
                <span class="text-sm font-semibold md:text-lg">Back to the top</span>
            </a>
        </nav>
        <article class="items-center border-t py-4 text-sm lg:flex"
            aria-label="{{ wp_get_nav_menu_name('footer_navigation') }}">
            <p class="font-normal">
                &copy; {{ $year }}
                <a class="hover:text-blue-800" href="{{ home_url('/') }}"
                    title="{{ $website }}">{{ $website }}</a>
            </p>
        </article>
    </x-section>
</footer>
