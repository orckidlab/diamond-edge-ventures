@if ($layout->pages)
    <x-section class="bg-dark-900">
        <x-grid class="grid-cols-1 gap-6 md:grid-cols-1 md:gap-14 lg:grid-cols-4">
            <x-article class="mb-12 text-white md:mb-0">
                {!! $layout->description !!}
            </x-article>
            @foreach ($layout->pages as $page)
                @php
                    $ID = $page->ID;
                    $title = $page->post_title;
                    $excerpt = $page->post_excerpt;
                    $associatedCategories = get_the_category($ID);
                    $firstAssociatedCategory = reset($associatedCategories);
                    $icon = get_field('svg_icon', $firstAssociatedCategory);
                    $color = get_field('tailwind_class', $firstAssociatedCategory);
                @endphp
                <a class="group text-white transition-all" href="{{ get_permalink($page->ID) }}"
                    title="{{ $title }}">
                    <figure class="flex-column flex flex-wrap space-y-8">
                        @if ($icon)
                            <picture
                                class="inline-flex aspect-square w-20 flex-col content-center items-center justify-center text-white">
                                @svg($icon, 'f-full h-full object-cover', ['aria-label' => $title])
                            </picture>
                        @endif
                        <figcaption>
                            <h4 class="mb-2 font-medium lg:text-xl">{{ $title }}</h4>
                            {!! $excerpt !!}
                        </figcaption>
                    </figure>
                    <x-buttons>
                        <x-button
                            class="text-{{ $color }} {{ $firstAssociatedCategory->slug == 'strategy' ? 'brightness-200' : '' }} !px-0"
                            color="transparent" tag="button">
                            <span class="mr-3 transition-all group-hover:mr-6">
                                Learn more about {{ $title }}
                            </span>
                            @svg('images.icons.x-link', 'w-5 h-auto', ['aria-label' => $title])
                        </x-button>
                    </x-buttons>
                </a>
            @endforeach
        </x-grid>
    </x-section>
@endif
