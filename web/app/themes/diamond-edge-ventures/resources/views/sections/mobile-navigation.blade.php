<transition enter-from-class="translate-x-[150%] opacity-0" leave-to-class="translate-x-[150%] opacity-0"
    enter-active-class="transition duration-300" leave-active-class="transition duration-300">
    <aside class="mobile-navigation fixed top-0 right-0 z-50 h-screen w-full overflow-y-auto bg-white shadow-xl lg:w-80"
        role="navigation" aria-label="mobile navigation" v-cloak v-show="mobileNavigation">
        <nav class="flex flex-col">
            <button class="button icon-only ml-auto !p-5 mobile-nav-close" type="button" @click="toggleMobileNav">
                @svg('images.icons.x-mark', '!w-4 !h-4', ['aria-label' => 'Close navigation'])
            </button>
            @if ($primaryNavigation)
                @include('partials.nav-list', [
                    'items' => $primaryNavigation,
                    'class' => 'mobile-navigation grid grid-cols-1',
                ])
            @endif
            </article>
            <nav class="p-5">
                @if ($socialsNavigation)
                    @include('partials.navigations.socials', ['links' => $socialsNavigation])
                @endif
                <x-buttons class="mt-8">
                    <x-button tag="a" url="/contact-us">Contact us</x-button>
                </x-buttons>
            </nav>
        </nav>
    </aside>
</transition>
