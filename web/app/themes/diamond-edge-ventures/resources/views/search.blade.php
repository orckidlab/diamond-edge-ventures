@extends('layouts.app')

@section('content')
    <x-section>
        <x-breadcrumb></x-breadcrumb>
        @include('partials.page-header')

        <header class="flex w-full content-center justify-between">
            @include('partials.search-form', ['postType' => 'post', 'placeholder' => 'Search news'])
        </header>

        @if (!have_posts())
            <x-alert type="default">
                {!! __('Sorry, no results were found.', 'sage') !!}
            </x-alert>
        @endif

        <x-article class="text-white">
            <x-grid
                class="not-prose !mt-12 grid-cols-1 gap-6 gap-y-6 will-change-auto max-sm:relative max-sm:flex max-sm:w-full max-sm:snap-x max-sm:snap-mandatory max-sm:overflow-x-auto max-sm:px-8 md:grid-cols-2 lg:grid-cols-3">
                @while (have_posts())
                    @php(the_post())
                    @includeFirst(['partials.content-' . get_post_type(), 'partials.content'])
                @endwhile
            </x-grid>
        </x-article>

        <footer class="heir-h2:hidden heir-a:hover:text-amber-400 mt-6">
            {!! the_posts_navigation([
                'prev_text' => '← Prev',
                'next_text' => 'Next →',
                'screen_reader_text' => 'Posts Navigation',
            ]) !!}
        </footer>

    </x-section>
@endsection
