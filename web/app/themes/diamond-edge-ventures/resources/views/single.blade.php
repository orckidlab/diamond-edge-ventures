@extends('layouts.app', ['position' => $headerType != 'banner' ? 'sticky' : 'fixed', 'categorySlug' => $category->slug ?? 'general'])

@section('content')
    @while (have_posts())
        @php(the_post())
        @if ($headerType)
            @include('sections.hero')
        @endif

        @if (!empty(get_the_content()))
            <x-section>
                @includeFirst(['partials.content-single-' . get_post_type(), 'partials.content-single'])
            </x-section>
        @endif

        @if ($sections)
            {{-- Development only (show fields name to build template) --}}
            @env('local')
            <x-section class="bg-neutral-100">
                <x-article>
                    <h3>JSON extract from the dynamic sections</h3>
                    <pre class="!prose-sm">{{ json_encode($sections, JSON_PRETTY_PRINT) }}</pre>
                </x-article>
            </x-section>
            @endenv
            @include('sections.flexible', ['color' => $color, 'fillColor' => $fillColor])
        @endif
    @endwhile
@endsection
