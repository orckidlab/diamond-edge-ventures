{{--
  Template Name: Search
--}}

@extends('layouts.app')

@section('content')
    <x-section>
        <x-breadcrumb></x-breadcrumb>
        @include('partials.page-header')
        <header class="w-full content-center justify-between lg:flex lg:space-x-20">
            @include('partials.search-form', ['placeholder' => 'Search website'])
        </header>
    </x-section>
@endsection
