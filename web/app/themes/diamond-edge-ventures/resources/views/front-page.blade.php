{{--
  Template Name: Front Page
--}}

@extends('layouts.app')

@section('content')
    @while(have_posts()) @php the_post() @endphp
    <section class="relative">
        <h1 class="absolute top-0  bottom-0 m-auto drop-shadow text-center w-full  right-0 md:text-left md:pl-5 h-fit font-bold md:max-w-[80%] xl:max-w-[1536px] left-0 text-5xl xl:text-7xl text-white z-10">
            <span class="block xl:max-w-[700px]">{!! get_the_title() !!}</span>
        </h1>
        <img src="{{ $thumbnail }}" alt=""
        class="object-cover h-[400px] md:h-auto w-full">
    </section>
    <x-section class="py-8 xl:py-12 bg-[#F5F6FA]" id="who-we-are">
        <x-article class="">
            <div class="max-w-[1080px] flex m-auto items-center flex-col">
                <span class='text-center text-[24px] leading-[40px] xl:text-[38px] xl:leading-[62px]'>{!! $counter['headline'] !!} <b class=''>Mitsubishi Chemical Group</b></span>
                <div class="grid gap-8 grid-cols-1 md:grid-cols-3 w-full justify-between hidden">
                    <div class="text-center">
                        <h2 class="text-[#0364AD] text-center !mt-11 !mb-0 font-medium !text-[40px] xl:!text-[68px]">{!! $counter['counter_1']['value'] !!}</h2>
                        <span class="text-xl text-[#707070]">{!! $counter['counter_1']['label'] !!}</span>
                    </div>
                    <div class="text-center">
                        <h2 class="text-[#0364AD] text-center !mt-11 !mb-0 font-medium !text-[40px] xl:!text-[68px]">{!! $counter['counter_2']['value'] !!}</h2>
                        <span class="text-xl text-[#707070]">{!! $counter['counter_2']['label'] !!}</span>
                    </div>
                    <div class="text-center">
                        <h2 class="text-[#0364AD] text-center !mt-11 !mb-0 font-medium !text-[40px] xl:!text-[68px]">{!! $counter['counter_3']['value'] !!}</h2>
                        <span class="text-xl text-[#707070]">{!! $counter['counter_3']['label'] !!}</span>
                    </div>
                </div>
            </div>
        </x-article>
    </x-section>
    @if($investments['title'])
    <x-section class="py-8 xl:py-8 " id="focus-area">
      <x-article>
        <div class="max-md:max-w-[600px] xl:max-w-[1000px] flex m-auto items-center flex-col">
          <h3 class="!text-[24px] xl:!text-[38px] !mb-4 !mt-0">{!! $investments['title'] !!}</h3>
          <div class="!text-[18px] xl:!text-[24px]">{!! $investments['content'] !!}</div>
          <div class="grid gap-8 grid-cols-2 xl:grid-cols-4 w-full mt-10">
            @foreach ($investments['areas'] as $key => $area)
            <div class="text-center flex flex-col justify-center  items-center">
              <img src="{!! $area['image']['url'] !!}" class="!mb-1" alt="">
              <h3 class="text-[#707070] !text-lg font-normal max-w-[188px]">{!! $area['title'] !!}</h3>
            </div>
            @endforeach
          </div>
        </div>
      </x-article>
    </x-section>
    @endif
    @if($teams['team_category'])
    <x-section class="bg-[#F5F6FA]" id="team">
        <x-article>
            <?php $n = 0; $class='md:py-0';?>
            @foreach ($teams['team_category'] as $key => $team)
            @if($n != 0)
                <?php $class = 'mt-10 md:py-0';?>
            @endif
            <div class="{{ $class }}">
                <h3 class="!text-[24px] xl:!text-[38px] !mb-0 !mt-0">{!! $team['headline'] !!}</h3>
                @if($team['sub_headline'] != '')
                    <p>{!! $team['sub_headline'] !!}</p>
                @endif
                <div class="grid gap-8 grid-cols-1 md:grid-cols-2 xl:grid-cols-4">
                    @foreach ($team['team_member'] as $key => $member)
                    <div class="bg-white relative group/item ">
                        <span class="text-[20px] 2xl:text-[26px] p-6 pb-0 block">{{ $member['title'] }} <i class="text-[#0364ad] block">{{ $member['subtitle'] }}</i></span>
                        <img class="!my-0 !mr-0 !ml-auto" src="{{ $member['image']['url'] }}" alt="">
                        <div class="bg-[#0363aded] invisible group-hover/item:visible absolute top-0 left-0 h-full w-full text-white">
                            <div class="p-6 pb-0 flex">
                                <span class="text-[20px] 2xl:text-[26px] block w-4/5">{{ $member['title'] }} <i class="block">{{ $member['subtitle'] }}</i></span>
                                @if($member['linked_in'])
                                <a href="{{ $member['linked_in'] }}" target="_blank">
                                    @svg('images.icons.x-linkedin', 'w-8 h-8 mt-2', ['aria-label' => $website])
                                </a>
                                @endif
                            </div>
                            @if($member['description'])
                                <p class="px-6 leading-6 text-[13px] 2xl:text-[18px]">{!! $member['description'] !!}</p>
                            @endif
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
            <?php $n++; ?>
            @endforeach
        </x-article>
    </x-section>
    @endif

    @if($portfolios)
    <x-section class="!bg-white" id="portfolio">
        <x-article>
            <h3 class="!text-[24px] xl:!text-[38px] !mb-2">{!! $portfolios['portfolio_headline'] !!}</h3>
            <div class="grid gap-8 grid-cols-1 md:grid-cols-2 xl:grid-cols-4">
                @foreach ($portfolios['portfolio_items'] as $key => $portfolio)
                <div>
                    <div>
                        <img src="{{ $portfolio['image']['url'] }}" alt="">
                    </div>
                    <div class="2xl:text-[18px]">
                        <p class="min-h-[64px]"><b>Overview:</b> {{ $portfolio['overview'] }}</p>
                        @if($portfolio['hq_or_aquired_by'])
                            <p><b>Acquired by:</b> {{ $portfolio['exit_details'] }}</p>
                        @else
                            <p><b>HQ:</b> {{ $portfolio['headquarter'] }}</p>
                        @endif
                        @if($portfolio['website_url'])
                        <a href="{{ $portfolio['website_url'] }}" target="_blank" class="no-underline    group flex gap-3 items-center text-[18px] text-[#2b73ba] hover:text-[#1e2d54]"><span>View Site</span>
                            <svg class="group-hover:scale-125  transition-all" xmlns="http://www.w3.org/2000/svg" width="19.811" height="19.811" viewBox="0 0 19.811 19.811"> <path id="Path_40650" data-name="Path 40650" d="M13.5,6H5.25A2.25,2.25,0,0,0,3,8.25v10.5A2.25,2.25,0,0,0,5.25,21h10.5A2.25,2.25,0,0,0,18,18.75V10.5m-10.5,6L21,3m0,0H15.75M21,3V8.25" transform="translate(-2.25 -1.939)" fill="none" class="group-hover:stroke-[#1e2d54]" stroke="#2b73ba" stroke-linecap="round" stroke-linejoin="round" stroke-width="1.5"/></svg>
                        </a>
                        @endif
                    </div>
                </div>
                @endforeach
            </div>
        </x-article>
    </x-section>
    @endif

    <x-section id="contact" class="with-extended-header with-background is-primary contact bg-[#d3dfe3]">
        <x-article>
            <h2 class="!mb-3">{!! $contact_form['title'] !!}</h2>
            <p class="text-[14px] !mb-10">{!! $contact_form['subtitle'] !!}</p>
            <div class="wrapper">
                <div class="container">
                    <div class="content">
                        <div class="box">
                            <div class="columns is-multiline is-variable is-8">

                                <div class="column is-half">
                                    {!! do_shortcode($contact_form['shortcode']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </x-article>

    </x-section>
    @endwhile
@endsection