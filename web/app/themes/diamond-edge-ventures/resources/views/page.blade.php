@extends('layouts.app')

@section('content')
    @while (have_posts())
        @php(the_post())

        @if ($hero)
            @include('sections.hero')
        @endif

        @if ($sections)
            @include('sections.flexible')
        @endif
        {{-- @if ($hero)
            @include('sections.hero')
        @endif
        <x-section>
            @includeFirst(['partials.content-page', 'partials.content'])
        </x-section> --}}
    @endwhile
@endsection
