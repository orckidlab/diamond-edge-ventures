@extends('layouts.app')

@section('content')
    @include('partials.page-header', ['breadcrumb' => false])
    <x-section>
        @if (!have_posts())
            <x-alert type="default">
                {!! __('Sorry, but the page you are trying to view does not exist.', 'sage') !!}
            </x-alert>
            {{-- {!! get_search_form(false) !!} --}}
            <x-article>
                <p>It looks like you've reached a URL that doesn't exist. Please use the navigation above or buttons below
                    to
                    find
                    your way back to our a-maize-ing website.</p>
            </x-article>
            <x-buttons>
                <a class="button primary" href="{{ home_url('/') }}">Back to homepage</a>
            </x-buttons>
    </x-section>
    @endif
@endsection
