<?php

namespace App\View\Composers;

use Exception;
use Roots\Acorn\View\Composer;

class FrontPage extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'front-page'
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'thumbnail' => get_the_post_thumbnail_url( get_the_ID(), 'full' ),
            'counter' => get_field('counter'),
            'teams' => get_field('team'),
            'portfolios' => get_field('portfolio'),
            'contact_form' => get_field('contact_form')
        ];
    }

    private function getTeams()
    {
        $employees_items = get_posts([
            'post_type' => 'team',
            'posts_per_page'=> '-1'
        ]);

        return array_map(function ($post) {
            return (object)[
                'title' => $post->post_title,
                'content' => apply_filters('the_content', $post->post_content),
                'role' => get_field('role', $post->ID),
                'linkedin' => get_field('linkedin', $post->ID)
            ];
        }, $employees_items);
    }

    private function allPortfolios()
    {

        $query_args = [
            'post_type'      => 'portfolio',
            'post_status'    => 'publish',
            'posts_per_page' => -1,
            'orderby'        => 'publish_date'
        ];

        $items = get_posts($query_args);

        return array_map(function ($post) {
            return (object) [
                'title'     => $post->post_title,
                'content' => apply_filters('the_content', $post->post_content),
                'thumbnail' => get_the_post_thumbnail_url($post->ID, 'full'),
                'url'       => get_field('url', $post->ID),
            ];
        }, $items);
    }
}
