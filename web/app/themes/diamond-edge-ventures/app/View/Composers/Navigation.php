<?php

namespace App\View\Composers;

use Exception;
use Roots\Acorn\View\Composer;
use Log1x\Navi\Navi;

class Navigation extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'sections.header',
        'sections.mobile-navigation',
        'sections.footer'
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'logo' => get_template_directory_uri(  ) . '/public/images/brand/logo-diamond.png',
            'navigation' => fn ($slug, $class) => $this->getNavigation($slug, $class),
            'primaryNavigation' => $this->navi('primary'),
            'socialsNavigation' => $this->navi('socials'),
            'footerNavigation'  => $this->navi('footer'),
            'legalsNavigation' => $this->navi('legals')
        ];
    }

    /**
     * Returns the NAVI navigation.
     * @ref https://github.com/Log1x/navi
     * @return array
     */
    public function navi($slug)
    {
        $navigation = (new Navi())->build($slug);

        if ($navigation->isEmpty()) {
            return [];
        }

        return $navigation->toArray();
    }

    /**
     * Returns the WP navigation.
     * @ref https://developer.wordpress.org/reference/functions/wp_nav_menu/
     * @return string
     */
    public function getNavigation($slug, $class)
    {
        try {
            return wp_nav_menu([
                'theme_location' => $slug,
                'menu_class'     => 'navigation ' . $class
            ]);
        } catch (Exception $exception) {

            if (WP_ENV !== 'production') {
                return 'Undefined navigation';
            }
        }
    }
}
