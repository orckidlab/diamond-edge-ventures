<?php

namespace App\View\Composers;

use Exception;
use Roots\Acorn\View\Composer;

class Page extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'front-page',
        'default',
        'index',
        'single',
        'page',
        'page-*'
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {

        $headerType = get_field('type_of_page_header');
        $headerBackgroundImage = '';

        if ($headerType === 'banner') {
            $headerBackgroundImage = get_the_post_thumbnail_url(get_the_ID(), 'full');
        } elseif ($headerType === 'default') {
            $headerBackgroundImage = '/app/uploads/2023/02/default-hero.svg';
        }

        $headerArray = [
            'type' => $headerType,
            'background' => get_field('header_background_color'),
            'image' => $headerBackgroundImage,
            'conditional' => get_field('header_buttons_conditional'),
            'buttons' => get_field('header_buttons')
        ];

        $sections = get_field('sections');

        return [
            'hero' => (object) $headerArray,
            'sections' => $sections
        ];
    }
}
