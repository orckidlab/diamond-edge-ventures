<?php

namespace App\View\Composers;

use Roots\Acorn\View\Composer;

class Thumbnails extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        '*'
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'thumbnail'    => fn ($size)    => $this->getThumbnail($size),
            'thumbnailURL' => fn ($ID, $size) => get_the_post_thumbnail_url($ID, $size),
        ];
    }

    /**
     * Returns the post thumbnail.
     * @ref https://developer.wordpress.org/reference/functions/the_post_thumbnail/
     * @return string
     */

    public function getThumbnail($size)
    {
        return the_post_thumbnail($size);
    }

    /**
     * Returns the post thumbnail url.
     * @ref https://developer.wordpress.org/reference/functions/get_the_post_thumbnail_url/
     * @return string
     */

    public function getThumbnailURL($ID, $size = 'large')
    {
        return get_the_post_thumbnail_url($ID, $size);
    }
}
