<?php

namespace App\View\Composers;

use Roots\Acorn\View\Composer;

class Footer extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'sections.footer',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'year' => $this->year()
        ];
    }

    /**
     * Returns the current year.
     *
     * @return string
     */
    public function year()
    {
        return date('Y');
    }
}
