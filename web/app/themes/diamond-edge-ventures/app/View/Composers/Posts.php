<?php

namespace App\View\Composers;

use Roots\Acorn\View\Composer;

class Posts extends Composer
{
    /**
     * List of views served by this composer.
     *
     * @var array
     */
    protected static $views = [
        'front-page',
        'page',
        'page-*',
        'single',
        'sections.blogs',
    ];

    /**
     * Data to be passed to view before rendering.
     *
     * @return array
     */
    public function with()
    {
        return [
            'postsByCategory' => fn ($amount, $category) => $this->getByCategory($amount, $category),
        ];
    }

    /**
     * Returns latest posts with ability specify amount + category.
     *
     * @return array
     */
    public function getByCategory($amount, $category)
    {

        if ($category) {
            $query_args = [
                'post_type' => 'post',
                'post_status' => 'publish',
                'posts_per_page' => $amount,
                'no_found_rows' => true,
                'ignore_sticky_posts' => true,
                'orderby' => 'publish_date',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'category',
                        'field'    => 'slug',
                        'terms'    => array($category),
                        'operator' => 'IN'
                    ),
                ),
            ];
        } else {
            $query_args = [
                'post_type' => 'post',
                'post_status' => 'publish',
                'no_found_rows' => true,
                'ignore_sticky_posts' => true,
                'posts_per_page' => $amount,
                'orderby' => 'publish_date'
            ];
        }

        $items = get_posts($query_args);

        // dd($items);

        return array_map(
            fn ($post) =>
            (object)[
                'ID' => $post->ID,
                'url' => get_field('url', $post->ID),
                'title' => mb_strimwidth($post->post_title, 0, 50, '...'),
                'thumbnail' => get_the_post_thumbnail_url($post->ID, 'large')
            ],
            $items
        );
    }

    /**
     * Returns latest posts with ability to exclude and specify amount.
     *
     * @return array
     */
    public function getLatest($amount, $exclude)
    {

        $query_args = [
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => $amount,
            'orderby' => 'publish_date',
            'post__not_in' => array($exclude)
        ];

        $items = get_posts($query_args);

        return array_map(function ($post) {
            return (object)[
                'ID' => $post->ID,
                'title' => $post->post_title,
                'url' => get_permalink($post->ID),
                'excerpt' => get_the_excerpt($post->ID),
                'thumbnail' => get_the_post_thumbnail($post->ID, 'large'),
                'date' => get_the_date('F j, Y', $post->ID)
            ];
        }, $items);
    }

    /**
     * Returns all posts.
     *
     * @return array
     */
    public function getAll()
    {

        $query_args = [
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => -1,
            'orderby' => 'publish_date',
        ];

        $items = get_posts($query_args);

        return array_map(function ($post) {
            return (object)[
                'title' => $post->post_title,
                'url' => get_permalink($post->ID),
                'date' => get_the_date('M d, Y', $post->ID),
                'thumbnail' => get_the_post_thumbnail($post->ID, 'large')
            ];
        }, $items);
    }
}
