<?php

namespace App\View\Components;

use Roots\Acorn\View\Component;

class Button extends Component
{

    /**
     * The button tag.
     *
     * @var string
     */
    public $tag;


    /**
     * The button target.
     *
     * @var string
     */
    public $target;

    /**
     * The button color.
     *
     * @var string
     */
    public $color;

    /**
     * The button base.
     *
     * @var string
     */
    public $base;

    /**
     * The button title.
     *
     * @var string
     */
    public $title;

    /**
     * The button url.
     *
     * @var string
     */
    public $url;

    /**
     * The button types.
     *
     * @var array
     */
    public $colors = [
        'base' => 'relative inline-flex items-center justify-center content-center mb-6 mr-6 overflow-hidden text-sm xl:text-base font-bold rounded-md py-2 md:py-3 px-4 sm:px-7 transition-all',
        'default' => 'bg-blue-700 border-2 border-blue-700 text-white hover:bg-blue-900 hover:border-blue-900',
        'outline-dark' => 'bg-transparent text-blue-200 border-2 border-blue-200 hover:bg-blue-900 hover:border-blue-900 hover:text-white',
        'white' => 'bg-white border-2 border-white text-blue-200 hover:bg-blue-900 hover:border-blue-900 hover:text-white',
        'outline' => 'bg-transparent border-2 border-white text-white hover:bg-blue-900 hover:border-blue-900 hover:text-dark-600',
        'icon-only' => 'bg-transparent p-2.5 text-white text-lg hover:opacity-75',
        'transparent' => 'bg-transparent px-0 text-blue-200'
    ];

    /**
     * Create the button instance.
     *
     * @param  string  $type
     * @param  string  $color
     * @param  string  $title
     * @param  string  $url
     * @param  string  $target
     * @return void
     */
    public function __construct($tag = 'a', $color = 'default', $base = 'base', $title = null, $url = 'javascript:', $target = '')
    {
        $this->tag = $tag;
        $this->title = $title;
        $this->url = $url;
        $this->target = $target;
        $this->base = $this->colors[$base] ?? $this->colors['base'];
        $this->color = $this->colors[$color] ?? $this->colors['default'];
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\View\View|string
     */
    public function render()
    {
        return $this->view('components.button');
    }
}
