<?php

/**
 * Theme filters.
 */

namespace App;

/**
 * Add "… Continued" to the excerpt.
 *
 * @return string
 */
add_filter('excerpt_more', function () {
    return sprintf(' &hellip; <a href="%s">%s</a>', get_permalink(), __('Continued', 'sage'));
});

/*
 * Adding HTTP Security Headers
 *
 * */
add_filter('wp_headers', function () {
    $headers['Expect-CT'] = 'enforce, max-age=30';
    $headers['Referrer-Policy'] = 'no-referrer-when-downgrade';
    $headers['Server'] = get_bloginfo('name');
    return $headers;
});

/*
 * Adding JS Defer
 *
 * */
add_filter('script_loader_tag', function ($url) {

    if (is_user_logged_in()) {
        return $url;
    }

    if (FALSE === strpos($url, '.js')) {
        return $url;
    }

    if (strpos($url, 'jquery.min.js')) {
        return $url;
    }

    return str_replace(' src', ' defer src', $url);
}, 10);
