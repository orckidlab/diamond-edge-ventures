// https://tailwindcss.com/docs/configuration

const plugin = require('tailwindcss/plugin')

// Rotate X utilities
const rotateXY = plugin(function ({ addUtilities }) {
    addUtilities({
        '.rotate-y-0': {
            transform: 'rotateY(0deg)'
        },
        '.rotate-y-180': {
            transform: 'rotateY(180deg)'
        },
        '.rotate-negative-y-180': {
            transform: 'rotateY(-180deg)'
        },
        '.rotate-x-180': {
            transform: 'rotateX(180deg)'
        },
        '.rotate-negative-x-180': {
            transform: 'rotateX(-180deg)'
        }
    })
})

module.exports = {
    content: ['./index.php', './app/**/*.php', './resources/**/*.{php,vue,js}'],
    safelist: [
        'bg-blue-500',
        'bg-blue-300',
        'bg-split-blue-white-1',
        'bg-split-blue-white-2',
        'bg-split-blue-white-3'
    ],
    theme: {
        extend: {
            container: {
                center: true
            },
            fontFamily: {
                roboto: ['Roboto']
            },
            colors: {
                'blue-900': '#1E2D54',
                'blue-800': '#2A62A8',
                'blue-700': '#0264AD',
                'blue-600': '#2B73BA',
                'blue-500': '#D3DFE3',
                'blue-400': '#202E53',
                'blue-300': '#F5F6FA',
                'blue-200': '#3F71B5',
                'blue-100': '#DEE2F5',
                'red-900': '#E3001B',
                'grey-400': '#D9DDEF',
                'grey-300': '#54575C',
                'grey-200': '#CAD9DD',
                'grey-100': '#ECEDF4'
            },
            backgroundImage: {
                'split-blue-white-1':
                    'linear-gradient(90deg, rgba(2,100,173,1) 75%, rgba(255,255,255,1) 25%)',
                'split-blue-white-2':
                    'linear-gradient(0deg, rgba(245, 246, 250, 1) 60%, rgba(255,255,255,1) 40%)',
                'split-blue-white-3':
                    'linear-gradient(0deg, rgba(2, 100, 173, 1) 60%, rgba(255,255,255,1) 40%)',
                hr: 'linear-gradient(to right, transparent, rgba(84, 87, 92, .2), transparent)',
                overlay:
                    'linear-gradient(0deg, rgba(0,0,0,0.7959384437368697) 0%, rgba(255,255,255,0) 100%)'
            },
            dropShadow: {
                '3xl': '0px 2px 46px rgba(188,188,188,0.3)'
            },
            typography: ({ theme }) => ({
                DEFAULT: {
                    css: {
                        '--tw-prose-bullets': theme('colors.blue[400]'),
                        '--tw-prose-body': '#000000',
                        '--tw-prose-links': theme('colors.blue[700]'),
                        '--tw-prose-invert-bullets': '#FFFFFF'
                    }
                }
            })
        }
    },
    plugins: [
        require('@tailwindcss/typography'),
        require('tailwind-children'),
        rotateXY
    ]
}
