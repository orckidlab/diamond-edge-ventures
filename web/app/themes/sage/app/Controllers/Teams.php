<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Teams extends Controller
{
    public static function get()
    {
        $employees_items = get_posts([
            'post_type' => 'team',
            'posts_per_page'=> '-1'
        ]);

        return array_map(function ($post) {
            return (object)[
                'title' => $post->post_title,
                'content' => apply_filters('the_content', $post->post_content),
                'role' => get_field('role', $post->ID),
                'linkedin' => get_field('linkedin', $post->ID)
            ];
        }, $employees_items);
    }
}
