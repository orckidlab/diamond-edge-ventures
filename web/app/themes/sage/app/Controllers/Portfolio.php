<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class Portfolio extends Controller
{

    public static function all()
    {

        $query_args = [
            'post_type'      => 'portfolio',
            'post_status'    => 'publish',
            'posts_per_page' => -1,
            'orderby'        => 'publish_date'
        ];

        $items = get_posts($query_args);

        return array_map(function ($post) {
            return (object) [
                'title'     => $post->post_title,
                'content' => apply_filters('the_content', $post->post_content),
                'thumbnail' => get_the_post_thumbnail_url($post->ID, 'full'),
                'url'       => get_field('url', $post->ID),
            ];
        }, $items);
    }
}
