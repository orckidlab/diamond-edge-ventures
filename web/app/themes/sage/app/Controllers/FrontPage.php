<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class FrontPage extends Controller
{
    public static function hero($post)
    {
        $background = null;
        $component  = null;
        $size       = 'is-large';

        if (has_post_thumbnail($post->ID)) {
            $image      = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail');
            $background = 'style="background-image: url(' . $image[0] . ')"';
        }

        return $component = 'class="hero is-primary ' . $size . '" ' . $background . '';
    }
}
