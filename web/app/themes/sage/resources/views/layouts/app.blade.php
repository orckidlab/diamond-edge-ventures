<!doctype html>
<html {!! get_language_attributes() !!}>
@include('partials.head')
<body @bodyclass>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-PRKPX79"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="app">
    <div id="global">
        @action('get_header')
        @include('partials.navigation')
        @include('partials.header')
        <main id="main-content" role="document">
            @yield('content')
        </main>
        @include('partials.footer')
    </div>
</div>
@wpfoot
@action('get_footer')
</body>
</html>
