<footer id="main-footer">
    <main class="container">
        <section>
            <nav>
                <ul>
                    <li>
                        ©{!! do_shortcode('[year]') !!}
                        <a href="{{ home_url('/') }}">{{ App::siteName() }}</a>
                    </li>
                    @if (has_nav_menu('secondary_navigation'))
                        {!! wp_nav_menu([
                            'theme_location' => 'secondary_navigation',
                            'menu_class' => 'nav',
                            'container' => '',
                            'container_class' => false,
                            'container_id' => '',
                            'items_wrap' => '%3$s'
                        ]) !!}
                    @endif
                </ul>
            </nav>
        </section>
    </main>
</footer>
