<section class="single-portfolio">
    <article class="portfolio content">
        <div class="information">
            <div class="columns is-mobile is-variable is-centered is-multiline is-8">
                <div class="column is-full-mobile is-half-desktop">
                    <figure style="background-image: url('{{ get_the_post_thumbnail_url($post->ID, 'full')  }}');"></figure>
                </div>
                <div class="column is-full-mobile is-half-desktop">
                    <div class="flex-content">
                        @if(get_field('custom_tag_list'))
                            <ul class="tag-list">
                                <li>{!! get_field('custom_tag_list') !!}</li>
                            </ul>
                        @else
                            <ul class="tag-list">
                                @foreach(get_the_terms($post->ID, 'deliverables') as $tag)
                                    <li>{{ $tag->name }}</li>
                                @endforeach
                            </ul>
                        @endif
                        <h1 class="has-text-centered">
                            {!! App::title() !!} <br/>
                        </h1>
                        @php the_content() @endphp
                    </div>
                </div>
            </div>
        </div>
        @php
            $images = get_field('gallery');
        @endphp
        @if( get_field('testimonial') )
            <div class="testimonial">
                <div class="content">
                    {!! get_field('testimonial') !!}
                </div>
            </div>
        @endif
        @if($images)
            <div class="gallery" v-cloak>
                <flick-slider ref="slider"
                              config="lodge"
                              @change="updateCount">
                    @foreach($images as $image)
                        <div class="carousel-cell">
                            <figure>
                                <img src="{{ $image['url'] }}" alt="{{ $image['alt'] }}"/>
                                <figcaption>
                                    <span>@{{ count }}/<?php echo count($images) ?></span>
                                    <span>{{ $image['alt'] }}</span>
                                </figcaption>
                            </figure>
                        </div>
                    @endforeach
                </flick-slider>
            </div>
        @endif
    </article>
    @if(get_next_post() || get_previous_post())
        <nav class="post-navigation content level">
            <div class="level-left prev">
                @if(get_next_post())
                    <h5>
                        <svg xmlns="http://www.w3.org/2000/svg" height="19" viewBox="0 0 19 19" width="19">
                            <path d="M0 0h24v24H0z" fill="none"/>
                            <path d="M15.41 7.41L14 6l-6 6 6 6 1.41-1.41L10.83 12z"/>
                        </svg>
                        <span>Previous story</span>
                    </h5>
                    @php next_post_link('%link', '%title'); @endphp
                @endif
            </div>
            <div class="level-right next">
                @if(get_previous_post())
                    <h5>
                        <span>Next story</span>
                        <svg xmlns="http://www.w3.org/2000/svg" height="19" viewBox="0 0 19 19" width="19">
                            <path d="M0 0h24v24H0z" fill="none"/>
                            <path d="M10 6L8.59 7.41 13.17 12l-4.58 4.59L10 18l6-6z"/>
                        </svg>
                    </h5>
                    @php previous_post_link('%link', '%title'); @endphp
                @endif
            </div>
        </nav>
    @endif
</section>
