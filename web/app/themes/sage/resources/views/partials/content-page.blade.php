<section>
    <main class="container">
        <article class="content">
            @php the_content() @endphp
            {!! wp_link_pages(['echo' => 0, 'before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']) !!}
        </article>
    </main>
</section>
