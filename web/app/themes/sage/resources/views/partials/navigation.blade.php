<transition v-cloak name="fade" enter-active-class="animated fadeIn faster"
            leave-active-class="animated fadeOut faster">
    <section class="mobile-navigation" role="navigation" v-cloak v-if="$navigation.active">
        <main>
            <nav class="navigation-wrapper">
                <button @click="$navigation.toggleNavigation" class="close delete is-large"></button>
                <div class="columns is-vcentered">
                    @if (has_nav_menu('primary_navigation'))
                        <div class="column">
                            <ul class="navigation primary">
                                {{-- {!! wp_nav_menu([
                                     'theme_location' => 'primary_navigation',
                                     'menu_class' => 'nav',
                                     'container' => '',
                                     'container_class' => false,
                                     'container_id' => '',
                                     'items_wrap' => '%3$s'
                                 ]) !!}--}}
                                <ul class="navigation primary">
                                    <li class="menu-item menu-who-we-are"><a @click="$navigation.toggleNavigation" href="#who-we-are">Who we are</a></li>
                                    <li class="menu-item menu-team"><a @click="$navigation.toggleNavigation" href="#team">Team</a></li>
                                    <li class="menu-item menu-portfolio"><a @click="$navigation.toggleNavigation" href="#portfolio">Portfolio</a></li>
                                    <li class="menu-item menu-contact"><a @click="$navigation.toggleNavigation" href="#contact">Contact</a></li>
                                </ul>
                            </ul>
                        </div>
                    @endif
                    <div class="column">
                        <div class="columns is-multiline">
                            <div class="column is-full-tablet is-half-desktop">
                                <h5 class="is-uppercase">Contact</h5>
                                <ul class="navigation secondary">
                                    <li><a href="mailto:info@diamondedge.vc" title="Email us at Info@diamondedge.vc">Info@diamondedge.vc</a></li>

                                </ul>
                                <address>
                                    <a title="Our location" href="https://www.google.com/maps/dir//2742+Sand+Hill+Rd,+Menlo+Park,+CA+94025,+USA/@37.4209615,-122.2126112,17z/data=!4m9!4m8!1m0!1m5!1m1!1s0x808fa5a1d0f94125:0x9f077c7dd4049826!2m2!1d-122.2104225!2d37.4209615!3e0" rel="noopener" target="_blank">
                                        2742 Sand Hill Road, <br/>
                                        Menlo Park, <br/>CA 94028 USA
                                    </a>
                                </address>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </main>
    </section>
</transition>
