<section class="hero is-medium default">
    <div class="hero-body">
        <main class="container">
            <div class="content">
                <h1 class="title">
                    {!! App::title() !!}
                </h1>
            </div>
        </main>
    </div>
</section>
