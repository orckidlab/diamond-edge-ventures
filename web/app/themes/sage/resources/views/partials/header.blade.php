<header id="main-header" class="has-background-white">
    <section>
        <main class="container">

            <nav class="navbar is-transparent" role="navigation" aria-label="main navigation">
                <a class="navbar-brand" href="{{ App::getHome() }}" title="{{ App::siteName() }}">
                    @svg('logos.brand')
                </a>
                <a @click="$navigation.toggleNavigation" role="button" class="navbar-burger is-flex" aria-label="menu" aria-expanded="false">
                    <span aria-hidden="true"></span>
                    <span aria-hidden="true"></span>
                </a>
                <div class="navbar-menu is-flex">
                    <ul class="navbar-end is-size-5">
                        @if (has_nav_menu('primary_navigation'))
                            {!! wp_nav_menu([
                                'theme_location' => 'primary_navigation',
                                'menu_class' => 'nav',
                                'container' => '',
                                'container_class' => false,
                                'container_id' => '',
                                'items_wrap' => '%3$s'
                            ]) !!}
                        @endif
                    </ul>
                </div>
            </nav>
        </main>
    </section>
</header>
