{{--
  Template Name: Front Page
--}}

@extends('layouts.app')

@section('content')
    @while(have_posts()) @php the_post() @endphp
    <section {!! FrontPage::hero($post) !!}>
        <div class="hero-body">
            <main class="container">
                <div class="columns is-mobile">
                    <div class="column is-gapless is-mobile-12 is-7-desktop is-7-fullhd">
                        <div class="content">
                            <h1 class="title">
                                {!! App::title() !!}
                            </h1>
                            @content
                        </div>
                    </div>
                </div>
            </main>
        </div>
    </section>
    <section id="who-we-are" class="with-extended-header">
        <div class="container">
            <div class="content">
                <h2>Who we are</h2>
            </div>
        </div>
        <div class="wrapper">
            <div class="container">
                <div class="content">
                    <div class="compact-content">
                        <figure class="responsive-image">
                            <img src="{!! get_field('image') !!}" alt="who-are-we"/>
                        </figure>
                        <div class="padded-content">
                            {!! get_field('who_are_we_title') !!}
                            <div class="buttons">
                                <a href="#contact" class="button is-primary">Contact Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="diverse-business-lines" class="with-extended-header with-pattern with-background is-primary">
        <div class="container">
            <div class="content">
                <h2>Diverse <br/>
                    Business Lines
                </h2>
            </div>
        </div>
        <div class="wrapper">
            <div class="container">
                <div class="content">
                    <div class="compact-content">
                        <div class="padded-content">
                            <h3>{{ get_field('headline') }}</h3>
                        </div>
                    </div>
                    <div class="columns is-multiline is-centered business-lines">
                        @foreach( get_field('items') as $item)
                            <div class="column">
                                <div class="card">
                                    <div class="card-image">
                                        <figure class="image">
                                            <img src="{!! $item['logo'] !!}" alt="{!! $item['title'] !!}" />
                                        </figure>
                                    </div>
                                    <div class="card-content">
                                        <h4>{!! $item['title'] !!}</h4>
                                        {!! $item['content'] !!}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="team" class="with-extended-header">
        <div class="container">
            <div class="content">
                <h2>Team</h2>
            </div>
        </div>
        <div class="wrapper">
            <div class="container">
                <div class="content">
                    <div class="columns is-multiline is-centered">
                        @foreach(Teams::get() as $team)
                            <div class="column">
                                <div class="card with-border">
                                    <div class="card-content">
                                        <h4 class="with-image">
                                            <a target="_blank" href="{{ $team->linkedin }}">
                                                <span>{{ $team->title }}</span>
                                                <i class="fab fa-linkedin"></i>
                                            </a>
                                        </h4>
                                        <h5>{{ $team->role }}</h5>
                                        {!! $team->content !!}
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="portfolio" class="with-extended-header with-background is-grey">
        <div class="container">
            <div class="content">
                <h2>Portfolio</h2>
            </div>
        </div>
        <div class="wrapper">
            <div class="container">
                <div class="content">
                    <div class="columns is-multiline">
                        @foreach(Portfolio::all() as $portfolio_item)
                            <div class="column is-half">
                                <div class="card with-quote">
                                    <div class="card-image">
                                        <figure class="image">
                                            <a target="_blank" href="{{ $portfolio_item->url }}">
                                                <img src="{{ $portfolio_item->thumbnail }}" alt="{{ $portfolio_item->title }}"/>
                                            </a>
                                        </figure>
                                    </div>
                                    <div class="card-content">
                                        {!! $portfolio_item->content  !!}
                                        <h6>{{ $portfolio_item->title }}</h6>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section id="contact" class="with-extended-header with-background is-primary contact">
        <div class="container">
            <div class="content">
                <h2>Contact</h2>
            </div>
        </div>
        <div class="wrapper">
            <div class="container">
                <div class="content">
                    <div class="box">
                        <div class="columns is-multiline is-variable is-8">
                            <div class="column is-half">
                                <h5>Email <br>
                                    <span><a href="mailto:Info@diamondedge.vc">Info@diamondedge.vc</a></span>
                                </h5>
                                <h5>Address <br>
                                    <span>
                                         <a target="_blank" href="https://www.google.com/maps/dir//2742+Sand+Hill+Rd,+Menlo+Park,+CA+94025,+USA/@37.4209615,-122.2126112,17z/data=!4m9!4m8!1m0!1m5!1m1!1s0x808fa5a1d0f94125:0x9f077c7dd4049826!2m2!1d-122.2104225!2d37.4209615!3e0">2742 Sand Hill Road, Menlo Park, CA 94025 USA</a></span>
                                </h5>
                                <iframe
                                    src="https://www.google.com/maps/embed?pb=!1m19!1m8!1m3!1d3168.6832303243496!2d-122.2126112!3d37.4209615!3m2!1i1024!2i768!4f13.1!4m8!3e0!4m0!4m5!1s0x808fa5a1d0f94125%3A0x9f077c7dd4049826!2s2742%20Sand%20Hill%20Rd%2C%20Menlo%20Park%2C%20CA%2094025%2C%20USA!3m2!1d37.4209615!2d-122.21042249999999!5e0!3m2!1sen!2suk!4v1597756841535!5m2!1sen!2suk"
                                    width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                            </div>
                            <div class="column is-half">
                                <h5>Contact Form</h5>
                                @shortcode('[forminator_form id="25"]')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endwhile
@endsection
